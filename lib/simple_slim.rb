require 'slimpay'

require 'simple_slim/mandate'
require 'simple_slim/direct_debit'
require 'simple_slim/direct_debit_issue'
require 'simple_slim/refund'

#
# Main SimpleSlim module
#
module SimpleSlim
end
